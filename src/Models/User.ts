export class User{
  uid:string;
  email:string;
  name:string;
  password:string;
  avatar:string;
  provider:string
}

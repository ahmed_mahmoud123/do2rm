import {Price} from './Price';
export class Item{
  id:string;
  name:string;
  barcode:string;
  image:string="assets/images/product_image.png";
  account_id:string;
}

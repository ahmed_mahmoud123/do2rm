export class Location{
  name:string="Egypt";
  latitude:number=30.062630;
  longitude:number=31.249670;
  marker:boolean=false;
}

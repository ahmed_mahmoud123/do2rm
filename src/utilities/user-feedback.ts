import {LoadingController,Loading,AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';


@Injectable()
export class UserFeedback{
  loader:Loading;
 constructor(public toastCtrl: ToastController,public alertCtrl:AlertController,public loadingCtrl:LoadingController){
 }


 start_progress(){
    this.loader=this.loadingCtrl.create({
       content: "إنتظر من فضلك ...",
       dismissOnPageChange:true
   });
   this.loader.present();
 }

 close_progress(){
   this.loader.dismiss();
 }
 alert(title:string,subtitle:string) {
    let msg = this.alertCtrl.create({
     title: title,
     subTitle: subtitle,
     buttons: ['Okay']
    });
    msg.present().catch(err=>{});
 }

 make_toast(msg:string,cssClass:string="toast"){
   let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass:cssClass
    });
    toast.present().catch(err=>{});
 }


 presentConfirm(cb) {
  let alert = this.alertCtrl.create({
    title: 'Confirm',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'Ok',
        handler: () => {
          cb();
        }
      }
    ]
  });
  alert.present().catch(err=>{});
}


}

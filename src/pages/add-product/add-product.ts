import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../Models/User';
import { Item } from '../../Models/Item';


@IonicPage()
@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html'
})


export class AddProductPage {
  user:User=new User();
  item:Item=new Item();
  tab:string='product';
  item_date_valid:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let item=navParams.get('item');
    if(item){
      this.item=item;
      this.tab='price';
    }
  }


  add_item_valid(valid){
    this.item_date_valid=valid;
  }

  item_change(item){
    this.item=item;
  }

}

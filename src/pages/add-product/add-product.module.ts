import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddProductPage } from './add-product';
import { AddPriceComponentModule } from '../../components/add-price/add-price.module';
import { AddProductComponent } from '../../components/add-product/add-product';

@NgModule({
  declarations: [
    AddProductPage,
    AddProductComponent
  ],
  imports: [
    IonicPageModule.forChild(AddProductPage),
    AddPriceComponentModule
  ],
  exports: [
    AddProductPage
  ]
})
export class AddProductPageModule {}

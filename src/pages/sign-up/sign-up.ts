import { Component,Input } from '@angular/core';
import { NavController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../providers/auth-service';
import { Camera } from '@ionic-native/camera';
import {DomSanitizer} from '@angular/platform-browser';
import {EmailValidator} from '../../validators/EmailValidator';
import { UserFeedback } from '../../utilities/user-feedback';
import { User } from '../../Models/User';
import { Storage } from '@ionic/storage';
import {LoginPage} from '../login/login';
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
  providers:[AuthService,Camera,UserFeedback]
})
export class SignupPage {
  signupform:FormGroup;
  avatar:string="assets/images/avatar.png";
  user:User=new User();
  @Input('update') update:boolean=false;
  constructor(private userFeedback:UserFeedback,private storage:Storage,private domSanitizer:DomSanitizer,private camera:Camera,private authService:AuthService,private formBuilder:FormBuilder,public navCtrl: NavController) {
    // create validation rules for signup form controllers
    let emailValidator=new EmailValidator(authService.http);

    this.signupform=formBuilder.group({
        name:['',Validators.compose([Validators.required])],
        password:['',Validators.compose([Validators.required,Validators.minLength(8)])],
        email:['',Validators.compose([Validators.required,,emailValidator.isValid])]
    });
    storage.get('user').then(user=>{
      if(user){
        this.user=user;
        this.user.email=user.account.email;
      }
    })
  }


  signup(){

    // if signup form data is valid validation rules
    if(this.signupform.valid){
      this.user=this.signupform.value;
      this.user.avatar=this.avatar;
        // collect data from the form and send it to auth service to call signup web service
        this.userFeedback.start_progress();
      this.authService.signup({account:this.signupform.value}).then(result=>{
        this.userFeedback.make_toast("تم التسجيل بنجاح","success");
        this.userFeedback.close_progress();
        this.storage.set('user',this.user).then(()=>{
          this.navCtrl.setRoot(LoginPage);
        });
      }).catch(err=>{
        this.userFeedback.make_toast("مشكله في التسجيل");
        this.userFeedback.close_progress();
      })
    }
  }

  get_avatar(){
    this.camera.getPicture({
      sourceType:0,
      quality: 100,
      targetWidth:300,
      targetHeight:300,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     this.avatar= 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });
  }


}

import { Component } from '@angular/core';
import { NavController,MenuController,ModalController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../providers/auth-service';
//import { SearchPage } from '../search/search';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import {ForgetPasswordPage} from '../forget-password/forget-password';
import {SignupPage} from '../sign-up/sign-up';
import {SearchPage} from '../search/search';
import { Storage } from '@ionic/storage';
import { User } from '../../Models/User';
import { UserFeedback } from '../../utilities/user-feedback';
import {EmailValidator} from '../../validators/EmailValidator';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[AuthService,Facebook,GooglePlus,UserFeedback]
})
export class LoginPage {
  loginform:FormGroup;
  user:User=new User();
  constructor(private userFeedback:UserFeedback,private storage:Storage,private modalCtrl:ModalController,private googlePlus:GooglePlus,private facebook:Facebook,private authService:AuthService,private menuCtrl:MenuController,private formBuilder:FormBuilder,public navCtrl: NavController) {
    this.menuCtrl.enable(false);
    let emailValidator=new EmailValidator(authService.http);
    // create validation rules for login form controllers
    this.loginform=formBuilder.group({
        email:['',Validators.compose([Validators.required,emailValidator.isValid])],
        password:['',Validators.compose([Validators.required,Validators.minLength(6)])]
    });
  }

  login(){
    // if login form data is valid validation rules
    if(this.loginform.valid){
        // collect data from the form and send it to auth service to call login web service
      this.userFeedback.start_progress();
      this.authService.login(this.loginform.value).then(res=>{
        this.user.uid=res.account.id["$oid"];
        this.storage.set('user',this.user).then(()=>{
          this.navCtrl.setRoot(SearchPage);
          this.userFeedback.close_progress();
        }).catch(err=>console.log(err));

      },err=>{
        this.userFeedback.close_progress();
        this.userFeedback.make_toast('تسجيل دخول خاطئ');
      });
    }
  }

  facebook_login(){
    // set scope permission for facebook application
    const permissions=['public_profile','email'];
    // request permiossions and get access token ..
    this.facebook.login(permissions).then(user=>{
      // send new request with access token to get user information and send empty params becuase we send using get method
      this.facebook.api("/me?fields=id,name,email,picture.width(300).height(300)",[]).then(user=>{
        // display user data
        this.user.name=user.name;
        this.user.avatar=user.picture.data.url;
        this.user.email=user.email;
        this.user.uid=user.id;
        this.user.password=user.id;
        this.user.provider="facebook"
        // Call Web service to perform login with facebook..;
        this.userFeedback.start_progress();
        this.authService.facebook_login({account:this.user}).then(user=>{
          this.user.uid=user.account.id["$oid"];
          this.storage.set('user',user);
          this.userFeedback.close_progress();
          this.navCtrl.setRoot(SearchPage);
        },err=>{
          this.userFeedback.close_progress();
          this.userFeedback.make_toast('تسجيل دخول خاطئ');
        }).catch(err=>{
          this.userFeedback.close_progress();
          this.userFeedback.make_toast('تسجيل دخول خاطئ');
        })
      }).catch(err=>{alert(JSON.stringify(err));})
    }).catch(err=>{alert(JSON.stringify(err));});
  }


  google_login(){
    // call google api to request accept premissions to get user info
    this.userFeedback.start_progress();
    this.googlePlus.login({scopes:"profile"}).then(user=>{
      // display user info
      this.user.name=`${user.givenName} ${user.familyName}`;
      this.user.uid=user.userId;
      this.user.password=user.userId;
      this.user.email=user.email;
      this.user.avatar=user.imageUrl;
      this.user.provider="google_oauth2"
      // Call Web service to perform login with facebook..;
      this.authService.google_login(this.user).then(result=>{
        this.user.uid=result.account.id["$oid"];
        this.storage.set('user',user).then(()=>{
          this.userFeedback.close_progress();
          this.navCtrl.setRoot(SearchPage);
        });
      })
    }).catch(err=>{
      this.userFeedback.close_progress();
      this.userFeedback.make_toast('تسجيل دخول خاطئ');
    })
  }


  forget_password(){
    let forgetModal=this.modalCtrl.create(ForgetPasswordPage);
    forgetModal.present();
  }

  signup(){
    this.navCtrl.push(SignupPage);
  }

}

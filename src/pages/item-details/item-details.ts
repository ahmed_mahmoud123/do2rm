import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import { ItemsService } from '../../providers/items-service';
import {AddProductPage} from '../add-product/add-product';
import { Item } from '../../Models/Item';
/**
 * Generated class for the ItemDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-item-details',
  templateUrl: 'item-details.html',
  providers:[ItemsService]
})
export class ItemDetailsPage {
  item:Item=new Item();
  prices:Array<any>=[];
  spinner_show:boolean=true;
  constructor(private itemsService:ItemsService,private domSanitizer:DomSanitizer,public navCtrl: NavController, public navParams: NavParams) {
    this.item.name=navParams.get('name');
    this.item.image=navParams.get('avatar');
    this.itemsService.get_prices(navParams.get('item_id')).then(prices=>{
      this.prices=prices;
      this.spinner_show=false;
    }).catch(err=>this.spinner_show=false);
  }

  goBack(){
    this.navCtrl.pop();
  }


  add_price(){

    this.navCtrl.push(AddProductPage,{
      item:this.item
    });
  }

}

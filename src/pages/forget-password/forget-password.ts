import { Component } from '@angular/core';
import { NavController,MenuController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../providers/auth-service';
import {EmailValidator} from '../../validators/EmailValidator';

@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
  providers:[AuthService]
})
export class ForgetPasswordPage {
  forgetform:FormGroup;
  constructor(private authService:AuthService,private formBuilder:FormBuilder,public navCtrl: NavController) {
    let emailValidator=new EmailValidator(authService.http);
    // create validation rules for login form controllers
    this.forgetform=formBuilder.group({
        email:['',Validators.compose([Validators.required,emailValidator.isValid]),emailValidator.isEmailExists]
    })
  }

  recovery(){
    // if email is valid and exists on server call forget_password api..
    if(this.forgetform.valid){
      this.authService.forget_password(this.forgetform.value).then(result=>{
        
      }).catch(err=>{

      })
    }
  }
}

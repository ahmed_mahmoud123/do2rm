import { Component } from '@angular/core';
import { ModalController,IonicPage, NavController,MenuController, NavParams } from 'ionic-angular';
import {AddProductPage} from '../add-product/add-product';
import {ItemDetailsPage} from '../item-details/item-details';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { ItemsService } from '../../providers/items-service';
import {DomSanitizer} from '@angular/platform-browser';

/**
 * Generated class for the SearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
  providers:[BarcodeScanner,SpeechRecognition,ItemsService]
})
export class SearchPage {
  items:Array<any>=[];
  searchKey:string;
  spinner_show:boolean=false;
  constructor(public modalCtrl: ModalController,private menuController:MenuController,private domSanitizer:DomSanitizer,private itemsService:ItemsService,private speechRecognition:SpeechRecognition,private barcodeScanner:BarcodeScanner,public navCtrl: NavController, public navParams: NavParams) {
    this.menuController.enable(true);
    this.do_search("");
  }

  add_product(){
      this.navCtrl.push(AddProductPage)
  }

  do_search(searchKey){
    this.items=[];
    this.spinner_show=true;
    this.itemsService.search(searchKey).then(items=>{
      this.items=items;
      this.spinner_show=false;
      if(items.length==1){
        let item=items.pop();
        this.item_details(item.id["$oid"],item.name,item.images[0]);
      }
    }).catch(err=>{
      this.spinner_show=false;
    });
  }
  scan_barcode(){
    this.barcodeScanner.scan({
      prompt:"جاري البحث"
    }).then(data=>{
      if(!data.cancelled){
        this.searchKey=data.text;
        this.do_search(data.text);
      }
    }).catch(err=>{});
  }

  speech_recognition(){
    const options={
      language:"ar-EG",
      prompt:"من فضلك تكلم"
    };
    this.speechRecognition.hasPermission()
    .then((hasPermission: boolean) =>{
      if(!hasPermission){
        this.speechRecognition.requestPermission().then(()=> this.speechRecognition.startListening(options)
        .subscribe((matches: Array<string>) => {
          if(matches.length){
            this.searchKey=matches.shift();
            this.do_search(this.searchKey);
          }
        }));
      }else{
        this.speechRecognition.startListening(options)
        .subscribe((matches: Array<string>) => {
          this.searchKey=matches.shift();
          this.do_search(this.searchKey);
        });
      }
    })
  }

  item_details(item_id,name,avatar){
    let itemDetailsModel = this.modalCtrl.create(ItemDetailsPage,
    { item_id: item_id,name:name,avatar:avatar },{showBackdrop:true,enableBackdropDismiss:true});
    itemDetailsModel.present();
  }


  doRefresh(refresher){
    this.itemsService.search("").then(items=>{
      this.items=items;
      refresher.complete();
    }).catch(err=>refresher.complete());
  }
}

import { Component,Output,Input,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { Item } from '../../Models/Item';
import {DomSanitizer} from '@angular/platform-browser';

/**
 * Generated class for the AddProductComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'add-product',
  templateUrl: 'add-product.html',
  providers:[BarcodeScanner,Camera]
})
export class AddProductComponent {

  text: string;
  addItemForm:FormGroup;
  @Input('item') item:Item;
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() itemChange: EventEmitter<Item> = new EventEmitter<Item>();

  constructor(private formBuilder:FormBuilder,private camera:Camera,private barcodeScanner:BarcodeScanner,private domSanitizer:DomSanitizer) {
    this.addItemForm=formBuilder.group({
        name:['',Validators.compose([Validators.required])],
        barcode:['',Validators.compose([Validators.required])]
    });
    this.addItemForm.valueChanges.subscribe(data =>{
      this.change.emit(this.addItemForm.valid);
      this.itemChange.emit(this.item);
    });
  }

  scan_barcode(){
    this.barcodeScanner.scan({
      prompt:"جاري البحث"
    }).then(data=>{
      if(!data.cancelled){
        this.addItemForm.controls.barcode.setValue(data.text);
      }
    }).catch(err=>{});
  }

  get_avatar(){
    this.camera.getPicture({
      //sourceType:0,
      quality: 100,
      targetWidth:430,
      targetHeight:110,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG
    }).then((imageData) => {
     this.item.image= 'data:image/jpeg;base64,' + imageData;
     this.itemChange.emit(this.item);
    }, (err) => {});
  }
}

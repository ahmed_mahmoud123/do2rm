import { Component,Input } from '@angular/core';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';
import { Events } from 'ionic-angular';

declare var google:any;

@Component({
  selector: 'google-map',
  templateUrl: 'google-map.html',
  providers:[GoogleMaps]
})
export class GoogleMapComponent {


  marker:any;
  infowindow:any;
  constructor(private events:Events,private googleMaps: GoogleMaps) {
    events.subscribe('get_location',(locationData)=>{
      if(locationData.marker){
        if(this.marker){
          this.marker.setPosition(new google.maps.LatLng(locationData.latitude, locationData.longitude));
          this.marker.setVisible(false);
          this.infowindow.setContent(locationData.name);
        }
      }else{
        this.loadMap(locationData.longitude,locationData.latitude,locationData.name);
      }
    })
  }

  ngAfterViewInit() {

  }

  loadMap(long,lat,location_name) {
    let self=this;
    var mapOptions = {
     center: { lat: lat, lng: long },
     zoom: 13,
     disableDefaultUI: true,// DISABLE MAP TYPE
     scrollwheel: false
    };
    var map = new google.maps.Map(document.getElementById('map'),mapOptions);
    var input = /** @type {HTMLInputElement} */ (document.getElementById('pac-input'));
   // Create the autocomplete helper, and associate it with
   // an HTML text input box.
   var autocomplete = new google.maps.places.Autocomplete(input);
   autocomplete.bindTo('bounds', map);
   map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
   this.infowindow = new google.maps.InfoWindow();
   var image = 'assets/marker.png';
   self.marker = new google.maps.Marker({
     position: new google.maps.LatLng(lat, long),
     map: map,
     icon: image
   });
   this.infowindow.setContent(`<strong>${location_name}</strong>`);
   google.maps.event.addListener(self.marker, 'click', function() {
     self.infowindow.open(map, self.marker);
   });
 // Get the full place details when the user selects a place from the
 // list of suggestions.
   google.maps.event.addListener(autocomplete, 'place_changed', function() {
   self.infowindow.close();
   var place = autocomplete.getPlace();
   if (!place.geometry) {
     return;
   }
   if (place.geometry.viewport) {
     map.fitBounds(place.geometry.viewport);
   } else {
     map.setCenter(place.geometry.location);
     map.setZoom(50);
   }
   // Set the position of the marker using the place ID and location.
   self.marker.setPlace(({
     placeId: place.place_id,
     location: place.geometry.location
   }));

   self.events.publish('item_location',{
     lat:place.geometry.location.lat(),
     lng:place.geometry.location.lng(),
     name:place.formatted_address
   });
   self.marker.setVisible(true);
   self.infowindow.setContent('<div>'+place.formatted_address + '</div>');
   self.infowindow.open(map, self.marker);
   });
 }

}

import { Component,Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { Events } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { DatePicker } from '@ionic-native/date-picker';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Item } from '../../Models/Item';
import { Price } from '../../Models/Price';
import { Location } from '../../Models/Location';

import { UserFeedback } from '../../utilities/user-feedback';

import { ItemsService } from '../../providers/items-service';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AddPriceComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'add-price',
  templateUrl: 'add-price.html',
  providers:[Geolocation,ItemsService,NativeGeocoder,DatePicker,Diagnostic,UserFeedback]
})
export class AddPriceComponent {

  text: string;
  addPriceForm:FormGroup;
  @Input('item') item:Item;
  date:string="";
  price:Price=new Price();
  uid:string;
  location:Location;
  constructor(private events:Events,private userFeedback:UserFeedback,private storage:Storage,private itemsService:ItemsService,private formBuilder:FormBuilder,private diagnostic:Diagnostic,private datePicker:DatePicker,private nativeGeocoder:NativeGeocoder,private geolocation:Geolocation) {
    this.addPriceForm=formBuilder.group({
        price:['',Validators.compose([Validators.required,Validators.pattern('[0-9]*')])],
        datetime:[]
    });

    this.get_location(false);
    this.events.subscribe('item_location',(location)=>{
      this.location.latitude=location.lat;
      this.location.longitude=location.lng;
      this.location.name=location.name;
    })
  }
  ngAfterViewInit() {
    setTimeout(()=>{
      let date:Date=this.price.date;
      this.date=`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
      this.storage.get('user').then(user=>{
        this.item.account_id=user.uid;
      });
    });
  }
  get_location(switchTo:boolean=true,marker=false){
    this.location=new Location();
    this.location.marker=marker;
    this.diagnostic.isLocationEnabled().then(result=>{
      if(result){
        this.geolocation.getCurrentPosition().then((resp) => {
          this.location.latitude=resp.coords.latitude;
          this.location.longitude=resp.coords.longitude;
          this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
          .then((result: NativeGeocoderReverseResult) => {
            result.city=result.city?result.city:'';
            result.district=result.district?result.district:'';
            this.location.name=`${result.street}  ${result.city}  ${result.district} ${result.countryName}`
            this.events.publish('get_location',this.location);
          })
          .catch((error: any) => {
            this.events.publish('get_location',this.location);
            this.userFeedback.make_toast("لم نتمكن من الحصول علي المكان")
          });
        }).catch(error=>{
          this.userFeedback.make_toast("لم نتمكن من الحصول علي المكان");
          this.events.publish('get_location',this.location);
        });
      }else{
        this.events.publish('get_location',this.location);
        this.userFeedback.make_toast("نرجو تشغيل خدمة تحديد الاماكن");
        if(switchTo)this.diagnostic.switchToLocationSettings();
      }
    })
  }

  get_datetime(){
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.price.date=date;
        this.date=`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  addItem(){
    if(this.addPriceForm.valid){
      this.userFeedback.start_progress();
      delete this.location['marker'];
      this.itemsService.addItem({item:this.item,location:this.location,price:this.price}).then(result=>{
        this.userFeedback.close_progress();
        this.userFeedback.make_toast("تمت الاضافه بنجاح","success");
      },err=>{
        this.userFeedback.close_progress();
        this.userFeedback.make_toast("يوجد مشكلة في الاضافه");
      }).catch(err=>{
        this.userFeedback.close_progress();
        this.userFeedback.make_toast("يوجد مشكلة في الاضافه");
      });
    }
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPriceComponent } from './add-price';
import {GoogleMapComponent} from '../google-map/google-map';

@NgModule({
  declarations: [
    AddPriceComponent,
    GoogleMapComponent
  ],
  imports: [
    IonicPageModule.forChild(AddPriceComponent),
  ],
  exports: [
    AddPriceComponent
  ]
})
export class AddPriceComponentModule {}

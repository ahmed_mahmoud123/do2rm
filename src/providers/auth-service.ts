import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {
  headers:Headers;
  api_url:string="/api"
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
    this.api_url="http://api.do2rm.com/v1";
  }

  signup(user){
    return this.http.post(`${this.api_url}/register`,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  login(user){
      return this.http.post(`${this.api_url}/authenticate`,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  facebook_login(user){
    return this.http.post(`${this.api_url}/register`,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  google_login(user){
    return this.http.post(`${this.api_url}/register`,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  forget_password(user){
    return this.http.post("ws_url",JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  updateProfile(user){
     this.http.post("ws_url",JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

}

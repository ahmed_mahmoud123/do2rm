import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ItemsService {
  headers:Headers;
  api_url:string="/api";
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
    this.api_url="http://api.do2rm.com/v1";
  }

  search(searchKey){
    return this.http.get(`${this.api_url}/items/search?q=${searchKey}`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  addItem(item){
    return this.http.post(`${this.api_url}/items`,JSON.stringify(item),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  get_prices(item_id){
    return this.http.get(`${this.api_url}/items/${item_id}/prices`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }



}

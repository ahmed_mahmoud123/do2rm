import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { ItemDetailsPageModule } from '../pages/item-details/item-details.module';
import { SignupPage } from '../pages/sign-up/sign-up';
import { ForgetPasswordPage } from '../pages/forget-password/forget-password';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {SearchPageModule} from '../pages/search/search.module'
import {AddProductPageModule} from '../pages/add-product/add-product.module'
import {HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { AddProductComponent } from '../components/add-product/add-product';
import { AddPriceComponent } from '../components/add-price/add-price';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    ForgetPasswordPage,
    SignupPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: 'رجوع',
    }),
    IonicStorageModule.forRoot(),
    SearchPageModule,
    AddProductPageModule,
    ItemDetailsPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    ForgetPasswordPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

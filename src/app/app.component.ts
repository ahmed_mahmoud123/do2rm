import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { AddProductPage } from '../pages/add-product/add-product';
import { SearchPage } from '../pages/search/search';

import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{title: string, component: any,isLogout?:boolean}>;

  constructor(private storage:Storage,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'الرئيسية', component: SearchPage },
      { title: 'الصفحة الشخصية', component: ProfilePage },
      { title: 'تسجيل الخروج', component: LoginPage,isLogout:true }
    ];
    this.storage.get('user').then(user=>{
      if(user)
        this.rootPage=SearchPage;
      else
        this.rootPage=LoginPage;
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if(page.isLogout)
      this.storage.clear().then(()=>{
        this.nav.setRoot(LoginPage);
      });
    else
    this.nav.setRoot(page.component);
  }

}

import { FormControl } from '@angular/forms';
import { AuthService } from '../providers/auth-service';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Rx";

export class EmailValidator {

  constructor(public http: Http) {}

   isValid(control: FormControl): any {
      var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if(!EMAIL_REGEXP.test(control.value)){
          return { "Please provide a valid email": true };
      }
      return null;
    }

    isEmailExists(control: any):any{
      return new Promise((resolve)=>{
        this.http.get("ws").map(resp=>resp.json()).subscribe(result=>{
          if(result.exists)
            resolve(null)
          else
            resolve({"Please provide exists email": true})
        })
      })
    }

    isEmailNotExists(control: any):any{
      return new Promise((resolve)=>{
        this.http.get("ws").map(resp=>resp.json()).subscribe(result=>{
          if(!result.exists)
            resolve(null)
          else
            resolve({"Please provide exists email": true})
        })
      })
    }

}
